from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from . import calculadora


def index(request):
    return HttpResponse("Hola, mundo. Estás en la página de inicio de tu app llamada calc.")

def operacion(request, operacion, operando1, operando2):
    try:
        ans = calculadora.dict[operacion](operando1, operando2)
    except ZeroDivisionError:
        ans = "No division by zero allowed"

    return HttpResponse("Result = {}".format(ans))