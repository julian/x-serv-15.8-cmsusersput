from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('<str:operacion>/<int:operando1>/<int:operando2>', views.operacion),
]